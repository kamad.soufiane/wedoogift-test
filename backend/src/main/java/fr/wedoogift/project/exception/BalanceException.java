package fr.wedoogift.project.exception;

/**
 * Balance exception
 */
public class BalanceException extends Exception  {

    public BalanceException(String message) {
        super(message);
    }
}
