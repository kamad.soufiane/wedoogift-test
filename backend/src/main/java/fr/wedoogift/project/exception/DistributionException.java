package fr.wedoogift.project.exception;

/**
 * Distribution exception
 */
public class DistributionException extends Exception {

    public DistributionException(String message) {
        super(message);
    }
}
