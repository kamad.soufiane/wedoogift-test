package fr.wedoogift.project.exception;

/**
 * Deposit exception
 */
public class DepositException extends Exception {

    public DepositException(String message) {
        super(message);
    }
}
