package fr.wedoogift.project.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Company class
 */
public class Company {

    private Long id;

    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public Company setId(Long id) {
        this.id = id;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Company setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public Company subtractValueFromBalance(BigDecimal value) {
        this.balance = this.balance.subtract(value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(id, company.id) && Objects.equals(balance, company.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", balance=" + balance +
                '}';
    }
}
