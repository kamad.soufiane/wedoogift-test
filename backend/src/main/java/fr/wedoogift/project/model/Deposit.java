package fr.wedoogift.project.model;

import fr.wedoogift.project.enumeration.DepositType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Deposit class
 */
public class Deposit {

    private Long id;

    private DepositType type;

    private BigDecimal amount;

    private LocalDate depositDate;

    private Long companyId;

    public Long getId() {
        return id;
    }

    public Deposit setId(Long id) {
        this.id = id;
        return this;
    }

    public DepositType getType() {
        return type;
    }

    public Deposit setType(DepositType type) {
        this.type = type;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Deposit setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public Deposit setCompanyId(Long companyId) {
        this.companyId = companyId;
        return this;
    }

    public LocalDate getDepositDate() {
        return depositDate;
    }

    public Deposit setDepositDate(LocalDate depositDate) {
        this.depositDate = depositDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return Objects.equals(id, deposit.id) && type == deposit.type && Objects.equals(amount, deposit.amount) && Objects.equals(depositDate, deposit.depositDate) && Objects.equals(companyId, deposit.companyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, amount, depositDate, companyId);
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "id=" + id +
                ", type=" + type +
                ", amount=" + amount +
                ", depositDate=" + depositDate +
                ", companyId=" + companyId +
                '}';
    }
}
