package fr.wedoogift.project.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * User class
 */
public class User {

    private Long id;

    private Set<Deposit> deposits = new HashSet<>();

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public Set<Deposit> getDeposits() {
        return deposits;
    }

    public User setDeposits(Set<Deposit> deposits) {
        this.deposits = deposits;
        return this;
    }

    public User addDeposit(Deposit deposit) {
        this.deposits.add(deposit);
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(deposits, user.deposits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, deposits);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", total of deposits=" + deposits.size() +
                '}';
    }
}