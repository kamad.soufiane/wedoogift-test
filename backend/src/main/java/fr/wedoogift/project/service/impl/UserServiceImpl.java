package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.exception.BalanceException;
import fr.wedoogift.project.factory.DepositServiceFactory;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.model.User;
import fr.wedoogift.project.service.DepositService;
import fr.wedoogift.project.service.UserService;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * User service implementation
 */
public class UserServiceImpl implements UserService {

    private DepositServiceFactory depositFactory = new DepositServiceFactory();

    @Override
    public BigDecimal calculateBalance(User user) throws BalanceException {
        LocalDate today = LocalDate.now();
        BigDecimal balance = BigDecimal.ZERO;
        try {
            for (Deposit deposit : user.getDeposits()) {
                DepositService depositService = depositFactory.getDepositService(deposit.getType());
                LocalDate expirationDate = depositService.getExpirationDate(deposit);
                if (today.isBefore(expirationDate)) {
                    balance = balance.add(deposit.getAmount());
                }
            }
            return balance;
        } catch (Exception e) {
            throw new BalanceException("Error calculating balance for user id=[" + user.getId() + "]");
        }
    }
}