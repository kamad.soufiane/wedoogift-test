package fr.wedoogift.project.service;

import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.model.Deposit;

import java.time.LocalDate;

/**
 * Deposit service
 */
public interface DepositService {

    /**
     * Function to calculate the expiration date of a deposit
     *
     * @param deposit
     * @return expiration date
     */
    LocalDate getExpirationDate(Deposit deposit) throws DepositException;
}
