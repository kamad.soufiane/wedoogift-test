package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.service.DepositService;

import java.time.LocalDate;

/**
 * Gift deposit service implementation
 */
public class GiftDepositServiceImpl implements DepositService {

    private static final int NUMBER_OF_DAYS_LIFESPAN = 365;

    @Override
    public LocalDate getExpirationDate(Deposit deposit) throws DepositException {
        if (DepositType.GIFT.equals(deposit.getType())) {
            return deposit.getDepositDate().plusDays(NUMBER_OF_DAYS_LIFESPAN);
        } else {
            throw new DepositException("Invalid DepositType :[" + deposit.getType() + "]");
        }
    }
}
