package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.exception.DistributionException;
import fr.wedoogift.project.model.Company;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.model.User;
import fr.wedoogift.project.service.CompanyService;

import java.time.LocalDate;

/**
 * Company service implementation
 */
public class CompanyServiceImpl implements CompanyService {

    @Override
    public User distributeDeposit(Deposit deposit, User user, Company company) throws DistributionException {
        if (company.getBalance().compareTo(deposit.getAmount()) == 1) {
                deposit.setDepositDate(LocalDate.now());
                user.addDeposit(deposit);
                company.subtractValueFromBalance(deposit.getAmount());
            return user;
        } else {
            throw new DistributionException("Error distributing deposit id=[" + deposit.getId() + "] from company id=["
                    + company.getId() + "] to user id=[" + user.getId() + "]");
        }
    }
}
