package fr.wedoogift.project.service;

import fr.wedoogift.project.exception.DistributionException;
import fr.wedoogift.project.model.Company;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.model.User;

/**
 * Company service
 */
public interface CompanyService {

    /**
     * Function allowing companies to distribute gift and meal deposits to a user
     *
     * @param deposit
     * @param user
     * @param company
     * @return concerned user
     * @throws DistributionException
     */
    User distributeDeposit(Deposit deposit, User user, Company company) throws DistributionException;
}
