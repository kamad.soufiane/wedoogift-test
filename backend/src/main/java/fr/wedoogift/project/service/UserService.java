package fr.wedoogift.project.service;


import fr.wedoogift.project.exception.BalanceException;
import fr.wedoogift.project.model.User;

import java.math.BigDecimal;

/**
 * User service
 */
public interface UserService {

    /**
     * Function to calculate the user's balance
     *
     * @param user
     * @return calculated balance
     * @throws BalanceException
     */
    BigDecimal calculateBalance(User user) throws BalanceException;
}
