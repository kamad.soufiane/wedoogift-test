package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.service.DepositService;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;

/**
 * Meal deposit service implementation
 */
public class MealDepositServiceImpl implements DepositService {

    @Override
    public LocalDate getExpirationDate(Deposit deposit) throws DepositException {
        if (DepositType.MEAL.equals(deposit.getType())) {
            int day = deposit.getDepositDate().getDayOfMonth();
            int month = deposit.getDepositDate().getMonthValue();
            int year = deposit.getDepositDate().getYear();
            return LocalDate.of(month <= 2 ? year : year + 1, Month.FEBRUARY, day).with(TemporalAdjusters.lastDayOfMonth());
        } else {
            throw new DepositException("Invalid DepositType :[" + deposit.getType() + "]");
        }
    }
}
