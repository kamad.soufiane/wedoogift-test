package fr.wedoogift.project.enumeration;

/**
 * Deposit type enumeration
 */
public enum DepositType {
    GIFT, MEAL
}
