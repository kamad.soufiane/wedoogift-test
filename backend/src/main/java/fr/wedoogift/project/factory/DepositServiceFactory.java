package fr.wedoogift.project.factory;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.service.DepositService;
import fr.wedoogift.project.service.impl.GiftDepositServiceImpl;
import fr.wedoogift.project.service.impl.MealDepositServiceImpl;

/**
 * Deposit service factory
 */
public class DepositServiceFactory {

    /**
     * Build service from deposit type
     *
     * @param depositType
     * @return adequate deposit service
     * @throws DepositException
     */
    public DepositService getDepositService(DepositType depositType) throws DepositException {
        switch (depositType) {
            case GIFT :
                return new GiftDepositServiceImpl();
            case MEAL :
                return new MealDepositServiceImpl();
            default:
                throw new DepositException("Invalid DepositType :[" + depositType + "]");
        }
    }
}