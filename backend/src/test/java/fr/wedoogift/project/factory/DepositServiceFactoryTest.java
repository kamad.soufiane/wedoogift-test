package fr.wedoogift.project.factory;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.service.DepositService;
import fr.wedoogift.project.service.impl.GiftDepositServiceImpl;
import fr.wedoogift.project.service.impl.MealDepositServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Deposit service factory test
 */
public class DepositServiceFactoryTest {

    private DepositServiceFactory depositFactory;

    @Before
    public void setup() {
        depositFactory = new DepositServiceFactory();
    }

    @Test
    public void getDepositService_giftDepositTypeCase_returnGiftDepositServiceImpl() throws DepositException {
        DepositService depositService = depositFactory.getDepositService(DepositType.GIFT);
        Assert.assertTrue(depositService instanceof GiftDepositServiceImpl);
    }

    @Test
    public void getDepositService_mealDepositTypeCase_returnMealDepositServiceImpl() throws DepositException {
        DepositService depositService = depositFactory.getDepositService(DepositType.MEAL);
        Assert.assertTrue(depositService instanceof MealDepositServiceImpl);
    }

}
