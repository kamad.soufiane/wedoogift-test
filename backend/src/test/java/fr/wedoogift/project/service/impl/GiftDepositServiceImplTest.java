package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.service.DepositService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Gift deposit service implementation test
 */
public class GiftDepositServiceImplTest {

    private DepositService depositService;

    private Deposit deposit;

    private static final Long COMPANY_ID = 10L;
    private static final BigDecimal DEPOSIT_AMOUNT = BigDecimal.valueOf(120);
    private static final Long DEPOSIT_ID = 5L;
    private static final LocalDate DEPOSIT_DATE = LocalDate.of(2022, 1, 15);

    @Before
    public void setup() {
        depositService = new GiftDepositServiceImpl();
        deposit = new Deposit().setId(DEPOSIT_ID).setType(DepositType.GIFT)
                .setAmount(DEPOSIT_AMOUNT).setCompanyId(COMPANY_ID).setDepositDate(DEPOSIT_DATE);
    }

    @Test
    public void getExpirationDate_giftDepositTypeCase_returnDatePlus365Days() throws DepositException {
        LocalDate date = depositService.getExpirationDate(deposit);
        Assert.assertEquals(date.getDayOfMonth(), DEPOSIT_DATE.getDayOfMonth());
        Assert.assertEquals(date.getYear(), DEPOSIT_DATE.getYear() + 1);
        Assert.assertEquals(date.getMonthValue(), DEPOSIT_DATE.getMonthValue());
    }

    @Test(expected = DepositException.class)
    public void getExpirationDate_mealDepositTypeCase_throwsDepositException() throws DepositException {
        depositService.getExpirationDate(deposit.setType(DepositType.MEAL));
    }
}
