package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DistributionException;
import fr.wedoogift.project.model.Company;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.model.User;
import fr.wedoogift.project.service.CompanyService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Company service implementation test
 */
public class CompanyServiceImplTest {

    private CompanyService companyService;

    private Company company;

    private User user;

    private Deposit deposit;

    private static final Long COMPANY_ID = 10L;
    private static final BigDecimal COMPANY_BALANCE = BigDecimal.valueOf(15000);
    private static final BigDecimal DEPOSIT_AMOUNT = BigDecimal.valueOf(120);
    private static final Long DEPOSIT_ID = 5L;
    private static final Long USER_ID = 5L;

    @Before
    public void setup() {
        companyService = new CompanyServiceImpl();
        deposit = new Deposit().setId(DEPOSIT_ID).setType(DepositType.GIFT)
                .setAmount(DEPOSIT_AMOUNT).setCompanyId(COMPANY_ID);
        company = new Company().setId(COMPANY_ID).setBalance(COMPANY_BALANCE);
        user = new User().setId(USER_ID);

    }

    @Test
    public void distributeDeposit_nominalCase_returnUpdatedUser() throws DistributionException {
        int userDepositsSize = user.getDeposits().size();
        user = companyService.distributeDeposit(deposit, user, company);
        Assert.assertEquals(user.getDeposits().size(), userDepositsSize + 1);
        Assert.assertTrue(user.getDeposits().contains(deposit));
        Assert.assertEquals(company.getBalance(), COMPANY_BALANCE.subtract(DEPOSIT_AMOUNT));
        Assert.assertEquals(deposit.getDepositDate().getYear(), LocalDate.now().getYear());
        Assert.assertEquals(deposit.getDepositDate().getDayOfMonth(), LocalDate.now().getDayOfMonth());
        Assert.assertEquals(deposit.getDepositDate().getMonthValue(), LocalDate.now().getMonthValue());
    }

    @Test(expected = DistributionException.class)
    public void distributeDeposit_depositAmountBiggerThanCompanyBalanceCase_throwsDistributionException() throws DistributionException {
        deposit.setAmount(company.getBalance().add(BigDecimal.ONE));
        int userDepositsSize = user.getDeposits().size();
        user = companyService.distributeDeposit(deposit, user, company);
        Assert.assertEquals(user.getDeposits().size(), userDepositsSize);
        Assert.assertFalse(user.getDeposits().contains(deposit));
        Assert.assertEquals(company.getBalance(), COMPANY_BALANCE);
        Assert.assertNull(deposit.getDepositDate());
    }

}
