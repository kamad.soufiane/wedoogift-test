package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.BalanceException;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.model.User;
import fr.wedoogift.project.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * User service implementation test
 */
public class UserServiceImplTest {

    private UserService userService;

    private User user;

    private static final int EXPIRED_AMOUNT = 150;
    private static final int DEPOSIT_AMOUNT_1 = 180;
    private static final int DEPOSIT_AMOUNT_2 = 200;

    @Before
    public void setup() {
        userService = new UserServiceImpl();
        Deposit expiredDeposit = new Deposit().setId(1L).setType(DepositType.MEAL)
                .setAmount(BigDecimal.valueOf(EXPIRED_AMOUNT)).setDepositDate(LocalDate.now().minusMonths(15));
        Deposit deposit1 = new Deposit().setId(2L).setType(DepositType.GIFT)
                .setAmount(BigDecimal.valueOf(DEPOSIT_AMOUNT_1)).setDepositDate(LocalDate.now().minusMonths(2));
        Deposit deposit2 = new Deposit().setId(3L).setType(DepositType.GIFT)
                .setAmount(BigDecimal.valueOf(DEPOSIT_AMOUNT_2)).setDepositDate(LocalDate.now().minusMonths(2));
        Set<Deposit> deposits = new HashSet<>();
        deposits.add(expiredDeposit);
        deposits.add(deposit1);
        deposits.add(deposit2);
        user = new User().setId(1L).setDeposits(deposits);
    }

    @Test
    public void calculateBalance_nominalCase_returnUsersBalance() throws BalanceException {
        BigDecimal balance = userService.calculateBalance(user);
        Assert.assertEquals(balance, BigDecimal.valueOf(DEPOSIT_AMOUNT_1 + DEPOSIT_AMOUNT_2));
    }

    @Test(expected = BalanceException.class)
    public void getExpirationDate_giftDepositTypeCase_throwsDepositException() throws BalanceException {
        Deposit deposit3 = new Deposit();
        user.addDeposit(deposit3);
        userService.calculateBalance(user);
    }
}
