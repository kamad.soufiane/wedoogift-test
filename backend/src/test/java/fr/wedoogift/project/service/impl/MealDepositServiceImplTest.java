package fr.wedoogift.project.service.impl;

import fr.wedoogift.project.enumeration.DepositType;
import fr.wedoogift.project.exception.DepositException;
import fr.wedoogift.project.model.Deposit;
import fr.wedoogift.project.service.DepositService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Meal deposit service implementation test
 */
public class MealDepositServiceImplTest {

    private DepositService depositService;

    private Deposit deposit;

    private static final Long COMPANY_ID = 10L;
    private static final BigDecimal DEPOSIT_AMOUNT = BigDecimal.valueOf(120);
    private static final Long DEPOSIT_ID = 5L;

    @Before
    public void setup() {
        depositService = new MealDepositServiceImpl();
        deposit = new Deposit().setId(DEPOSIT_ID).setType(DepositType.MEAL)
                .setAmount(DEPOSIT_AMOUNT).setCompanyId(COMPANY_ID);
    }

    @Test
    public void getExpirationDate_mealDepositTypeCase1_returnNextEndOfFebruary() throws DepositException {
        LocalDate date = depositService.getExpirationDate(deposit.setDepositDate(LocalDate.of(2022, 1, 15)));
        Assert.assertEquals(date.getDayOfMonth(), 28);
        Assert.assertEquals(date.getMonthValue(), 2);
        Assert.assertEquals(date.getYear(), 2022);
    }

    @Test
    public void getExpirationDate_mealDepositTypeCase2_returnNextEndOfFebruary() throws DepositException {
        LocalDate date = depositService.getExpirationDate(deposit.setDepositDate(LocalDate.of(2023, 10, 15)));
        Assert.assertEquals(date.getDayOfMonth(), 29);
        Assert.assertEquals(date.getMonthValue(), 2);
        Assert.assertEquals(date.getYear(), 2024);
    }

    @Test(expected = DepositException.class)
    public void getExpirationDate_giftDepositTypeCase_throwsDepositException() throws DepositException {
        depositService.getExpirationDate(deposit.setType(DepositType.GIFT));
    }
}
